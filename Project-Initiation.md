# New Project Initiation

## Content Learning Objectives

By the end of this activity, participants will be able to...

- Read a product description and begin a basic requirements analysis.
- Create a new project on GitLab, with required files and a board structure
to support Scrum development
- Begin work on an initial team Definition of Done.

## Process Skill Goals

During the activity, students should make progress toward:

- Applying the Scrum methodology to a new project.

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Project Description

Your team has been hired to create a new software product. Your customer
(Professor Wurst) would like a program that can convert the contents of a
file from Markdown to HTML. The project is to be developed on GitLab, using
the Scrum development methodology, and will be developed in increments. It
will be written in Java, with tests written in JUnit, and with documentation
written in Markdown. The code will be readable and well structured,following
the suggestions of Clean Code. The development environment will be VSCode,
with a Dev Container (so that it can be developed on any platform), have
automated builds, linting, and testing. It is to be licensed under an Open
Source license. The final product should be released as a Docker image so
that the user does not need to install Java on their own machine to run it. Version numbers will follow the Semantic Versioning standard.

### Questions (15 min)

1. As a team, read the description above and write any questions you have
for the customer about what needs to be built and how.

    1.

2. Propose a name for the product.

## Model 2: Create a GitLab Project

Your team will need its own GitLab project to work in.

### Steps (10 min)

1. In your team subgroup, click the `New Project` button.
2. Choose `Create a blank project`.
3. Enter your `Project name`.
4. Ignore `Project deployment target (optional)`.
5. Leave `Visibility Level` as `Private` (you have no choice.)
6. Leave `Initialize repository with a README` checked.
7. Click the `Create project` button.
8. Clone the project to all of your computers.

**Note:** Your team will be sharing this single project (not making your
own forks). You will still be using a branching workflow, but you will all
be able to push your branches to the same project. You will still make merge
requests to the main branch, but you will review and merge each others' work.
More details in later activities.

## Model 3: Set Up The Project's Board

Your team will need an Issue Board where you can keep track of the Product
Backlog, the Sprint Backlog, the Done issues, and the steps in between.

### Steps (10 min)

1. From your new project (on GitLab), choose `Manage > Labels`.
2. Use the `New Label` button to create the following labels:

    1. Product Backlog
    2. Sprint Backlog
    3. In Process
    4. Needs Review
    5. Done

    **Note:** You can choose whatever colors you prefer for your labels.

3. Choose `Plan > Issue Boards`.
4. Use the `Create List` button to create new lists based on your labels.
Put them in the following order, between `Open` and `Closed`:

    1. Product Backlog
    2. Sprint Backlog
    3. In Process
    4. Needs Review
    5. Done

    **Note:** You can drag the lists to reorder them.

## Model 4: Develop An Initial Definition of Done

Your team will need a Definition of Done so that developers producing an
increment (and developers reviewing the work) will know when something is
done. This may evolve as you learn more about what you are doing, but we
need something to start with.

### Steps (10 min)

1. Read [What is a Definition of Done?](https://www.scrum.org/resources/what-definition-done).
2. Write an *initial* Definition of Done for your team. (Remember that this
can be updated later as you need to.)

## Model 5: Create Some Initial Issues

Your team will need some issues to work on.

### Steps (10 min)

1. From your `Development Issue Board`, click the `+` button on the `Sprint
Backlog` list to create the following issues:

    1. Add a `.gitignore` file.
    2. Add a `.gitattributes` file.
    3. Edit the `README.md` file.
    4. Add a `/docs/developer/DefinitionOfDone.md` file.

2. As a team work on the `Title` and `Description` of each issue to be sure
that everyone on your team understands what they would need to do if they
take on that issue.

---
Copyright © 2023 Karl R. Wurst. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
